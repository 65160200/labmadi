import java.util.Scanner;

public class TicTacToe {
    static char[][] tictac = { { '-', '-', '-' },
            { '-', '-', '-' },
            { '-', '-', '-' } };
    static int row, col;
    static char player = 'X';
    static int countTurn;

    public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(tictac[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Turn " + player + ": Please input Row Col: ");
        row = sc.nextInt();
        col = sc.nextInt();
        if (checkAlready(row, col)) {
            System.out.println("This spot alreay have tic");
            showTable();
            System.out.println("Please input RowCol again");
            inputRowCol();
        }
        tictac[row][col] = player;
    }

    public static void switchPlayer() {
        if (player == 'X')
            player = 'O';
        else
            player = 'X';
    }

    static boolean checkWinRow() {
        for (int c = 0; c < 3; c++) {
            if (tictac[row][c] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkWinCol() {
        for (int r = 0; r < 3; r++) {
            if (tictac[r][col] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCross1() {
        if (tictac[0][0] == player && tictac[1][1] == player && tictac[2][2] == player)
            return true;
        return false;
    }

    static boolean checkCross2() {
        if (tictac[0][2] == player && tictac[1][1] == player && tictac[2][0] == player)
            return true;
        return false;
    }

    static boolean checkWin() {
        if (checkWinCol() || checkWinRow() || checkCross1() || checkCross2())
            return true;
        return false;
    }

    static void showWin() {
        System.out.println(player + " Win?!?");
    }

    static void showWel() {
        System.out.println("Welcome to TicTacToe game");
    }

    static boolean checkAlready(int Row, int Col) {
        if (tictac[Row][Col] == 'X' || tictac[Row][Col] == 'O')
            return true;
        return false;
    }

    public static void main(String[] args) {
        showWel();
        for (countTurn = 1; countTurn < 10; countTurn++) {
            showTable();
            inputRowCol();
            if (checkWin()) {
                showTable();
                showWin();
                break;
            }
            switchPlayer();
        }
        if (countTurn == 10) {
            showTable();
            System.out.println("Player Draw");
        }
    }
}
